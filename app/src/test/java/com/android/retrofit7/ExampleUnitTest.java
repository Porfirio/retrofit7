package com.android.retrofit7;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import com.google.gson.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void TestCall() throws IOException {
        GetData service = RetrofitClient.getRetrofitInstance().create(GetData.class);
        Call<List<City>> call = service.getCity("Naples");
        System.out.println(call.request());
        List<City> vals = call.execute().body();
        System.out.println(vals.toString());
        

    }
}