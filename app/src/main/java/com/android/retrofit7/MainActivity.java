package com.android.retrofit7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import androidx.appcompat.widget.LinearLayoutCompat;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MyAdapter myAdapter;
    private RecyclerView myRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//Create a handler for the RetrofitInstance interface//

        GetData service = RetrofitClient.getRetrofitInstance().create(GetData.class);

        Call<List<City>> call = service.getCity("Naples");

//Execute the request asynchronously//

        call.enqueue(new Callback<List<City>>() {

            @Override

//Handle a successful response//

            public void onResponse(Call<List<City>> call, Response<List<City>> response) {
                loadDataList(response.body());
            }




            @Override

//Handle execution failures//

            public void onFailure(Call<List<City>> call, Throwable throwable) {

//If the request fails, then display the following toast//

                Toast.makeText(MainActivity.this, "Unable to load cities", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClickbtn(View v){
        Toast.makeText(this,"Ciao",Toast.LENGTH_LONG).show();
    }

public void newactivity(View v){
    Intent intent = new Intent(MainActivity.this, HelloActivity.class);
    startActivity(intent);

}
//Display the retrieved data as a list//

    private void loadDataList(List<City> CityList) {

//Get a reference to the RecyclerView//

        myRecyclerView = findViewById(R.id.myRecyclerView);
        myAdapter = new MyAdapter(CityList);

//Use a LinearLayoutManager with default vertical orientation//

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        myRecyclerView.setLayoutManager(layoutManager);

//Set the Adapter to the RecyclerView//

        myRecyclerView.setAdapter(myAdapter);
    }

}