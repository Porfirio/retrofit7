package com.android.retrofit7;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class HelloActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);

        Toast.makeText(HelloActivity.this, "Unable to load users", Toast.LENGTH_SHORT).show();
    }
}